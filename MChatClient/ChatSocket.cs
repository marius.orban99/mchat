﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace MChatClient
{
	public class ChatSocket
	{
		public TcpClient Client;
		public NetworkStream NetStream;
		public string ServerIP { get; private set; }
		public bool IsOpen { get; private set; }

		public ChatSocket(string ip, int port)
		{
			Client = new TcpClient();
			ServerIP = ip;
			DisconnectAsync();
			ConnectAsync(ip, port);
		}

		public void ConnectAsync(string ip, int port)
		{
			try
			{
				if (!IsOpen)
				{
					Client.Connect(ip, port);
					NetStream = Client.GetStream();
					IsOpen = true;
				}
			}
			catch(Exception e)
			{
				throw new Exception("Error connecting to server:\n" + e.Message);
			}
		}

		public void DisconnectAsync()
		{
			if(IsOpen)
			{
				NetStream.FlushAsync();
				Client.Close();
				IsOpen = false;
			}
		}

		public async Task<int> SendBytesAsync(string s)
		{
			byte[] s_bytes = Encoding.Unicode.GetBytes(s);
			await NetStream.WriteAsync(s_bytes, 0, s_bytes.Length);
			await NetStream.FlushAsync();
			return 1;
		}
	}
}
