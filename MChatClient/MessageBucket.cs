﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;

namespace MChatClient
{
	public class MessageBucket
	{
		string ServerAddress;
		string Owner;
		int ServerPort;
		Dictionary<string, Message> Messages;
		NetworkManager _net;

		public delegate void MessageReceiver(Message m);
		public event MessageReceiver MessageReceived;

		public MessageBucket(string owner, string serverAddress, int serverPort=NetworkManager.SERVER_PORT)
		{
			if (owner.Trim() == "" || serverAddress.Trim() == "")
			{
				throw new Exception("No owner or server address were given to the MessageBucket constructor");
			}

			ServerAddress = serverAddress;
			ServerPort = serverPort;
			Owner = owner;
			Messages = new Dictionary<string, Message>();
			_net = new NetworkManager();
			_net.ConnectToServer(ServerAddress, ServerPort);
		}

		public async Task SendMessageAsync(string m)
		{
			await _net.SendMessage(ServerAddress, new Message(Owner, m));
		}

		public void OnMessageReceived(string msg_json)
		{
			MessageReceived?.Invoke(Message.FromJSON(msg_json));
		}

		public void StartListenerThread()
		{
			Thread _listenerThread = new Thread(ListenForMessages);
			_listenerThread.Start();
		}

		void ListenForMessages()
		{
			while (true)
			{
				NetworkStream _sock = _net.Servers[ServerAddress].Client.GetStream();
				int _bufferSize = 0;
				_bufferSize = _net.Servers[ServerAddress].Client.ReceiveBufferSize;
				byte[] _rawBuffer = new byte[32000];
				_sock.ReadAsync(_rawBuffer, 0, _bufferSize);
				string msg = Encoding.Unicode.GetString(_rawBuffer);
				OnMessageReceived(msg);
			}
		}
	}
}
