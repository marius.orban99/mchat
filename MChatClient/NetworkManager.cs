﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MChatClient
{
    public class NetworkManager{

		public const int CLIENT_PORT = 30030;
		public const int SERVER_PORT = 30031;

		public Dictionary<string, ChatSocket> Servers { get; private set; }

		public NetworkManager()
		{
			Servers = new Dictionary<string, ChatSocket>();
		}

		public ChatSocket ConnectToServer(string server_ip, int server_port)
		{
			if(Servers.ContainsKey(server_ip))
			{
				throw new Exception("Client already connected to server " + server_ip);
			}

			Servers.Add(server_ip, new ChatSocket(server_ip, server_port));
			return Servers[server_ip];
		}

		public void DisconnectFromServer(string server_ip)
		{
			if (Servers.ContainsKey(server_ip))
			{
				Servers[server_ip].DisconnectAsync();
				Servers.Remove(server_ip);
			}
		}

		public async Task<int> SendMessage(string destination_server, Message m)
		{
			if (!Servers.ContainsKey(destination_server))
			{
				throw new Exception("Client not connected to server " + destination_server);
			}

			return await Servers[destination_server].SendBytesAsync(m.ToJSON());
		}
    }
}
