﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MChatClient
{
	public class Message
	{
		public string Sender { get; set; }
		public string Content { get; set; }
		public string Timestamp { get; private set; }

		public Message()
		{
			Sender = "";
			Content = "";
			Timestamp = "";
		}

		public Message(string sender, string content)
		{
			Sender = sender;
			Content = content;
			RefreshTimestamp();
		}

		public void RefreshTimestamp()
		{
			Timestamp = DateTime.Now.ToUniversalTime().ToLongTimeString();
		}

		public string ToJSON()
		{
			return JsonConvert.SerializeObject(this);
		}

		public static Message FromJSON(string json)
		{
			try
			{
				return JsonConvert.DeserializeObject<Message>(json);
			}
			catch(Exception e)
			{
				throw new Exception("An error oocured deserializing message:\n" + e.Message);
			}
		}
	}
}
