﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MChatClient;

namespace MChatGUI
{
	public partial class Chatroom : Form
	{
		string CurrentUser;
		MessageBucket Room;

		public Chatroom()
		{
			InitializeComponent();
		}

		public Chatroom(string user, string server_address, int port)
		{
			try
			{
				CurrentUser = user;
				Room = new MessageBucket(user, server_address, port);
				Room.MessageReceived += Message_Received;
				this.Text = "MChat - Logged in as '" + user + "' on server " + server_address;
				this.Show();
			}
			catch(Exception e)
			{
				MessageBox.Show("An error occured while joining chatroom:\n" + e.Message);
				Close();
			}

		}

		void Message_Received(MChatClient.Message m)
		{
			richTextBox1.Text += "<b>" + m.Sender + ":</b> " + m.Content + "\n";
		}

		private void Chatroom_Load(object sender, EventArgs e)
		{

		}
	}
}
