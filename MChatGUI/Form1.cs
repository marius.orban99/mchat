﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MChatClient;

namespace MChatGUI
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			textBox3.Text = NetworkManager.SERVER_PORT.ToString();
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{
			if(checkBox1.Checked)
			{
				textBox3.Enabled = false;
			}
			else
			{
				textBox3.Enabled = true;
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			button1.Text = "Connecting...";
			button1.Enabled = false;
			Chatroom room = new Chatroom(textBox1.Text, textBox2.Text, Convert.ToInt32(textBox3.Text));
			button1.Enabled = true;
			button1.Text = "Connect";
		}
	}
}
